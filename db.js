var fs = require('fs'),
    xml2js = require('xml2js'),
    sqlite3 = require('sqlite3').verbose();

var parser = new xml2js.Parser();
var db = new sqlite3.Database('places.db');
// select * from places where name like 'Gä%' order by typenr, name
db.serialize(function(){
  db.run('CREATE TABLE if not exists places (name TEXT, type TEXT, typenr INTEGER, lat NUMERIC, lon NUMERIC)');
  db.run('CREATE INDEX index_name ON places (name)');

  var stmt = db.prepare('INSERT INTO places VALUES(?,?,?,?,?)');

  fs.readFile(__dirname + '/places.xml', function(err, data) {
      parser.parseString(data, function (err, result) {

          //console.dir(result.osm.node.length);

          for(var i=0;i<result.osm.node.length;i++){
            var lat=0, lon=0, name="", type="", typenr=4;

            console.log('--- Start place ---');
            //console.log(result.osm.node[i].$.lat);
            lat = result.osm.node[i].$.lat;
            //console.log(result.osm.node[i].$.lon);
            lon = result.osm.node[i].$.lon;
  //console.log(result.osm.node[i].tag.length);
            for(var j=0;j<result.osm.node[i].tag.length;j++){
              //console.log('-       --' + result.osm.node[i].tag[j].$.k + ' ----- ' + result.osm.node[i].tag[j].$.v);
              if(result.osm.node[i].tag[j].$.k == 'name'){
                //console.log(result.osm.node[i].tag[j].$.v);
                name = result.osm.node[i].tag[j].$.v;
              }
              if(result.osm.node[i].tag[j].$.k == 'place'){
                //console.log(result.osm.node[i].tag[j].$.v);
                type = result.osm.node[i].tag[j].$.v;
                if(result.osm.node[i].tag[j].$.v == "city"){
                  typenr = 1;
                }
                else if(result.osm.node[i].tag[j].$.v == "town"){
                  typenr = 2;
                }
                else if(result.osm.node[i].tag[j].$.v == "village"){
                  typenr = 3;
                }
              }
            }
            console.log(lat + '-' + lon + '-' + name + '-' + type + '-' + typenr);
            stmt.run(name,type,typenr,lat,lon);
            //console.log(result.osm.node[i]);
            console.log('--- End place ---');
          }

          console.log('Done');
      });

      stmt.finalize();

      /*db.each("SELECT name FROM places", function(err, row) {
        console.log(row);
      });*/
  });



});
db.close();


/*fs.readFile(__dirname + '/places_small.xml', function(err, data) {
    parser.parseString(data, function (err, result) {

        //console.dir(result.osm.node.length);

        for(var i=0;i<result.osm.node.length;i++){

          console.log('--- Start place ---');
          console.log(result.osm.node[i].$.lat);
          console.log(result.osm.node[i].$.lon);
//console.log(result.osm.node[i].tag.length);
          for(var j=0;j<result.osm.node[i].tag.length;j++){
            //console.log('-       --' + result.osm.node[i].tag[j].$.k + ' ----- ' + result.osm.node[i].tag[j].$.v);
            if(result.osm.node[i].tag[j].$.k == 'name'){
              console.log(result.osm.node[i].tag[j].$.v);
            }
            if(result.osm.node[i].tag[j].$.k == 'place'){
              console.log(result.osm.node[i].tag[j].$.v);
            }
          }

          //console.log(result.osm.node[i]);
          console.log('--- End place ---');
        }

        console.log('Done');
    });
});
*/
